# DESCRIPTION 
Build a countdown timer SPA using React JS with controls to add time, speed
up

The user should be able to enter a # of minutes (positive integer) and
click a “Start” button to initialize the countdown.

While the countdown timer is active
. User can speed up / slow down the speed at the following rates:
 1.0X (normal speed, selected by default), 1.5X, 2X

. Play/ Pause feature
. Notification for counter end ( before particular interval )

# REQUIREMENTS 
- npm version 6.14.5
- react version 17.0.1

# STEPS TO RUN
- npm install --save
- npm start 