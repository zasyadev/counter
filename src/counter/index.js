import React from 'react'
import { useState, useEffect } from 'react';
import { CounterWrapper } from './counterWrapper.style';
import PageContent from './pageContent';

const Counter = () => {
    const [ minutes, setMinutes ] = useState(0);
    const [seconds, setSeconds ] =  useState(0);
    const [ time, setTime ] = useState('');
    const [totalSec, setTotalSec] = useState(0);
    const [myInterval, setMyInterval] = useState('');
    const [halfTimeString, setHalfTimeString] = useState('');
    const [timerText, setTimerText] = useState('normal');
    const [isPaused, setIsPaused] = useState(false);
    const [duration, setDuration] = useState(1000);
    const [blink, setBlink] = useState(false);
    const [leftSec, setLeftSec] = useState(0);
    const [run, setRun] = useState(false);

    useEffect(()=>{
      if(totalSec > 0) {
          if(leftSec > 0 ) {
              if(!isPaused) {
              setMyInterval(setInterval(() => {
    
          if (seconds > 0) {
              setSeconds(seconds - 1);
          }
          if (seconds == 0) {
            if (minutes == 0) {
                clearInterval(myInterval)
            } else {
                setMinutes(minutes - 1);
                setSeconds(59);
            }
          }
          
            if(leftSec == (totalSec/2)){ 
              setHalfTimeString('More than halfway there!');
            }
            if(leftSec <= 21) {
              setTimerText('danger');
            }
            if(leftSec <= 11) {
              if(!blink) {
                setHalfTimeString('');
              } else {
                setHalfTimeString('More than halfway there!');
              }
              setBlink(!blink);
            }
            setLeftSec(leftSec-1);
          }, duration));
      }
  } else { 
    if(!isPaused) {
      setTime('');
      setTotalSec(0);
      setBlink(false);
      setHalfTimeString('Time’s up!');
      document.getElementById("startbtn").disabled=false;
        setRun(false);
      }
      clearInterval(myInterval);
    }
  } else {
    document.getElementById("startbtn").disabled=false;
    clearInterval(myInterval)
  }
      
      return ()=> {
          clearInterval(myInterval);
        };
  }, [leftSec, isPaused ]);

    useEffect(()=>{
      if (totalSec == 0 ) {
            clearInterval(myInterval);
        if(run) {
          document.getElementById("startbtn").disabled=true;
        }
      }
    }, [totalSec]);

    useEffect(()=>{ 
            clearInterval(myInterval);
    }, [seconds]);
  

  const inputChange = (value) => { 
      const regex = /^[0-9\b]+$/;
      if (value === '' || regex.test(value) && value<100) {
          setTime(value);
          setTotalSec(0);
          setHalfTimeString('');
          setTimerText('');
          setSeconds(0);
          if(value == 0) {
              setMinutes(0);
          } else {
              setMinutes(value);
          }
          setRun(false);
          document.getElementById("startbtn").disabled=false;
      }
  }

  const start = () => {
    if(time != '' && time != 0) {
      setRun(true);
      setTotalSec(time*60);
      setLeftSec(time*60);
      document.getElementById("startbtn").disabled=true;
    }
    
  }

const onPause = () => {
  let pause = isPaused;
  if(run) {
    setIsPaused(pause => !pause);
    if(pause == false) { 
      setTotalSec(0);
      setLeftSec(0);
      document.getElementById("pauseBtn").innerHTML = `<i class="fa fa-play" aria-hidden="true" title="pause"></i>`;
    } else {
      setTotalSec(time*60);
      setLeftSec(minutes*60 + seconds);
      document.getElementById("pauseBtn").innerHTML = `<i class="fa fa-fw" aria-hidden="true" title="resume"></i>`;
    }
  }
}

    return (
      <CounterWrapper>
          <PageContent time={time} inputChange={inputChange} start={start} halfTimeString={halfTimeString}
          timerText={timerText} minutes={minutes} seconds={seconds} onPause={onPause} setDuration={setDuration}
          duration={duration} />
        
      </CounterWrapper>
    )
}
export default Counter;