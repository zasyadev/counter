import styled from 'styled-components';


const CounterWrapper = styled.div`
.counter_bg {
	margin: 0 auto;
	width: 100%;
	margin-top: 50px;
	border: 5px solid #dfdfdf;
	display: inline-block;
	padding: 50px;
	@media (max-width: 420px) {
		padding: 30px;
	}
	@media (max-width: 320px) {
		padding: 20px;
	}
}

.counter_search {
    float: left;
    width: 100%;
	text-align: center;
}

.counter_search label
{
	font-size:20px;
	font-weight:normal;
	@media (max-width: 767px) {
		width: 100%;
	}
	
}
.counter_search input {
    border: 1px solid #dfdfdf;
    padding: 11px 30px 15px 30px;
    width:120px;
}

.counter_search button {
    border-radius: 0px;
    background: #54b8a1;
    border: 0px;
     padding: 13px 23px 13px 23px;
    text-transform: uppercase;
    font-size: 16px;
	margin-left:10px;
}

.counter_time {
    float: left;
    width: 100%;
    margin-top: 30px;
	text-align: center;
	margin-bottom: 24px;
}

.counter_time span
{
	font-size:20px;
	color:black;
	font-style:italic;
	
}
.counter_btn h2 {
    font-size: 100px;
    font-weight: bold;
	margin-top: 10px;
	
	@media (max-width: 445px) and (min-width: 321px) {
		font-size: 90px;
	}
	@media (max-width: 320px) {
		font-size: 80px;
	}
}

.counter_btn button
{
	background:none;
	border:2px solid black;
	color:black;
	width:50px;
	height:50px;
	border-radius:100px;
	
}

.counter_bottom {
    width: 257px;
    margin: 0 auto;
    position: relative;
    clear: both;
}

.one_btn {
    background: #6c6c6d !important;
    color: white !important;
}

.one_btn_1 {
    width: 75px;
    height: 50px;
    text-align: center;
    margin: 0 auto;
    border: 2px solid black;
    background: none;
    padding-top: 8px;
    color: black;
    font-size: 19px;
	float:left;
	/* margin-left:10px; */
}
.danger {
  color: red !important;
}
#spanText {
  height: 10px;
}
.speed2{
  margin-left: 10px;
}

#pauseBtn {
	outline: none;
}

`;

export { CounterWrapper };