import React from 'react';
import clsx from 'clsx';

const PageContent = props => {
    const {time, inputChange, start, halfTimeString, timerText,
         minutes, seconds, onPause, setDuration, duration} = props;
    
    const changeDuration = e => {
      setDuration(e.target.value);
    }

    const change = e => {
      inputChange(e.target.value);
    }

    return (
    <div className="container">
        <div className="counter_bg">
          <div className="counter_search">
            <label>Countdown: </label>
            <input type="text" name="minutes" min="1" placeholder="(Min)"  value={time} onChange={change}/>
            <button type="button" id="startbtn" className="btn btn-primary" onClick={start}>Start</button>
          </div>

          <div className="counter_time">
            <div id="spanText"><span>{halfTimeString}</span></div>
            <div className="counter_btn">
              <h2 className={timerText}>{minutes < 10 ? 0+''+minutes : minutes}:{seconds < 10 ? 0+''+seconds : seconds}
                <button type="button" className="btn btn-primary" onClick={onPause} id="pauseBtn"><i className="fa fa-fw" aria-hidden="true" title="pause"></i></button>
                </h2>  
            </div>
          </div>

          <div className="counter_bottom">
            <button type="button" value={1000} className={clsx('btn', 'btn-primary', 'speed1', 'one_btn_1', {one_btn : duration==1000})} name="1x" onClick={changeDuration}>1x</button>
            <button type="button" value={750} className={clsx('btn', 'btn-primary', 'speed2', 'one_btn_1', {one_btn : duration==750}) } name="1.5x" onClick={changeDuration}>1.5x</button>
            <button type="button" value={500} className={clsx('btn', 'btn-primary', 'speed2', 'one_btn_1', {one_btn : duration==500})} name="2x" onClick={changeDuration}>2x</button>
          </div>

        </div>
        </div>
    )
}
export default PageContent;